!(function () {
    //Constructor function
    function Person(firstName, lastName, dob) {
      this.firstName = firstName;
      this.lastName = lastName;
      this.dob = new Date(dob);
    }
  
    Person.prototype.getBirthYear = function () {
      return this.dob.getFullYear();
    };
  
    Person.prototype.getFullName = function () {
      return `${this.firstName} `;
    };
  
  
    // Class 
    class Asmuo {
      constructor(firstName, lastName, dob) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = new Date(dob);
      }
  
        getBirthYear() {
            return this.dob.getFullYear();
        }
        getFullName() {
            return `${this.firstName} ${this.las}`;
        }
    }
  
  
    // Instantiate object
    const person1 = new Person("Lukas", "Daunoravicius", "14-07-1994");
    const person2 = new Person("Mantas", "Kriksciunas", "03-27-1999");
  
  
  
  
    console.log(person1.getBirthYear());
    console.log(person1.getFullName());
  
  
  
  })();